package com.a0bit.vauntlet;

/**
 * Created by Alaina Traxler on 2/7/2018.
 */

public class Constants {
    public static final String URL_GETPLATFORMS = "http://thegamesdb.net/api/GetPlatformsList.php";
    public static final String URL_GETGAMESBYPLATFORM = "http://thegamesdb.net/api/GetPlatformGames.php?";
    public static final String URL_GETGAMEBYID = "http://thegamesdb.net/api/GetGame.php?";

    public static final String FILE_VAUNTLETS = "VAUNTLETS.tmp";

    public static final String KEY_IGDB = "3b85c0e2f92354a896dd51c4a5f17e9d";
}

