package com.a0bit.vauntlet.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.a0bit.vauntlet.R;
import com.a0bit.vauntlet.adapters.GameListAdapter;
import com.a0bit.vauntlet.models.Game;
import com.a0bit.vauntlet.models.Vauntlet;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowVauntlet extends AppCompatActivity {
    @BindView(R.id.recyclerView_ShowVauntlet) RecyclerView mRecyclerView_ShowVauntlet;

    public GameListAdapter mGameAdapter;
    public ArrayList<Game> mGames = new ArrayList<>();
    ShowVauntlet mParent;
    Vauntlet mVauntlet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_vauntlet);
        ButterKnife.bind(this);
        mParent = this;

        mVauntlet = ((Vauntlet) getIntent().getExtras().getSerializable("VAUNTLET"));

        setUpRecyclerView();
    }

    public void setUpRecyclerView(){
        mGameAdapter = new GameListAdapter(mVauntlet.getGameList(), mParent, "ShowVauntlet");
        mRecyclerView_ShowVauntlet.setHasFixedSize(true);
        mRecyclerView_ShowVauntlet.setAdapter(mGameAdapter);
        mRecyclerView_ShowVauntlet.setLayoutManager(new LinearLayoutManager(this));
    }
}
