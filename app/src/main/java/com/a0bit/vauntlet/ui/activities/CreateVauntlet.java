package com.a0bit.vauntlet.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.a0bit.vauntlet.Constants;
import com.a0bit.vauntlet.R;
import com.a0bit.vauntlet.adapters.UniversalPagerAdapter;
import com.a0bit.vauntlet.models.Game;
import com.a0bit.vauntlet.models.Vauntlet;
import com.a0bit.vauntlet.ui.fragments.AddGamesFragment;
import com.a0bit.vauntlet.ui.fragments.EditVauntletFragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateVauntlet extends AppCompatActivity {
    @BindView(R.id.pager) ViewPager mPager;

    public CreateVauntlet mParent;

    private AddGamesFragment mAddGamesFragment = new AddGamesFragment();
    private EditVauntletFragment mEditVauntletFragment = new EditVauntletFragment();

    public ArrayList<Game> mGameList = new ArrayList<Game>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_vauntlet);
        ButterKnife.bind(this);

        mParent = this;
        mEditVauntletFragment.pairGameList(mGameList);

        setUpPagerAdaper();
    }

    public void setUpPagerAdaper(){
        ArrayList<String> tabTitles= new ArrayList<>(Arrays.asList("Add Games", "View Vauntlet"));
        int pageCount = 2;
        ArrayList<Fragment> fragments = new ArrayList<>(Arrays.asList(mAddGamesFragment, mEditVauntletFragment));

        UniversalPagerAdapter adapter = new UniversalPagerAdapter(getSupportFragmentManager(), pageCount, tabTitles, fragments);
        mPager.setAdapter(adapter);
    }

    public void addGame(Game _game){
        mGameList.add(_game);
        mEditVauntletFragment.updateGameListAdapter();
    }

    public void createVauntlet(String _name) throws IOException, ClassNotFoundException {
        Log.v(">>", mGameList.size() + "");
        Vauntlet vauntlet = new Vauntlet(_name, mGameList);
        ArrayList<Vauntlet> activeVauntlets= new ArrayList<Vauntlet>();

        File file = getFileStreamPath(Constants.FILE_VAUNTLETS);
        FileInputStream fis = openFileInput(Constants.FILE_VAUNTLETS);

        if(!file.createNewFile()){
            if(file.length() != 0){
                ObjectInputStream ois = new ObjectInputStream(fis);
                activeVauntlets = (ArrayList<Vauntlet>) ois.readObject();
                ois.close();
            }
        }

        activeVauntlets.add(vauntlet);

        Log.v(">>", "Before Intent");

        FileOutputStream fos = openFileOutput (Constants.FILE_VAUNTLETS, Context.MODE_PRIVATE);
        ObjectOutputStream oos = new ObjectOutputStream (fos);
        oos.writeObject(activeVauntlets);
        oos.close();
        fos.close();

        startActivity(new Intent(this, MainActivity.class));
    }
}

