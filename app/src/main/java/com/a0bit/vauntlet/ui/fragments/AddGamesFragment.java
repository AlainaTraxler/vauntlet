package com.a0bit.vauntlet.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Spinner;

import com.a0bit.vauntlet.R;
import com.a0bit.vauntlet.adapters.GameListAdapter;
import com.a0bit.vauntlet.models.Game;
import com.a0bit.vauntlet.services.IgdbService;
import com.a0bit.vauntlet.ui.activities.CreateVauntlet;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AddGamesFragment extends Fragment {
    @BindView(R.id.spinner_AddGames) Spinner mSpinner_AddGames;
    @BindView(R.id.searchView_AddGames) SearchView mSearchView_AddGames;
    @BindView(R.id.recyclerView_Gamelets) RecyclerView mRecyclerView_Games;

    private IgdbService api = new IgdbService();

    public ArrayList<String> mPlatforms = new ArrayList<>();
    public ArrayList<String> mPlatformIds = new ArrayList<>();
    private CreateVauntlet mParent;
    private AddGamesFragment mAddGamesFragment;

    public GameListAdapter mGameAdapter;
    public ArrayList<Game> mGames = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_games, container, false);
        ButterKnife.bind(this, view);
        mParent = (CreateVauntlet) getActivity();
        mAddGamesFragment = this;

        setUpRecyclerView();
        setUpSearchView();

        return view;
    }

    public void setUpSearchView(){
        mSearchView_AddGames.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                api.searchForGame(s, mParent, mAddGamesFragment);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
    }

//    public void filterGames(String query){
//        mFilteredGames.clear();
//        for(Game game : mGames){
//            if(game.getName().toLowerCase().contains(query)){
//                mFilteredGames.add(game);
//            }
//        }
//        mGameAdapter.notifyDataSetChanged();
//    }

    public void setUpRecyclerView(){
        mGameAdapter = new GameListAdapter(mGames, mParent, "AddGamesFragment");
        mRecyclerView_Games.setHasFixedSize(true);
        mRecyclerView_Games.setAdapter(mGameAdapter);
        mRecyclerView_Games.setLayoutManager(new LinearLayoutManager(this.getContext()));
    }

//    private void getPlatforms(){
//        GamesDbService apiService = new GamesDbService();
//
//        apiService.findAllPlatforms(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
////                Toast.makeText(CreateVauntlet.this, "Unable to fetch platforms from GamesDB", Toast.LENGTH_SHORT).show();
//                Log.e(">>>>",e.toString());
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                JSONObject jsonObj = null;
//                try {
//                    jsonObj = XML.toJSONObject(response.body().string());
//                    JSONArray rawPlatforms = jsonObj.getJSONObject("Data").getJSONObject("Platforms").getJSONArray("Platform");
//                    for(int i = 0; i < rawPlatforms.length(); i++){
//                        String name = rawPlatforms.getJSONObject(i).getString("name");
//                        String id = rawPlatforms.getJSONObject(i).getString("id");
//
//                        mPlatforms.add(name);
//                        mPlatformIds.add(id);
//                    }
//                    Log.d(">>>>>", mPlatforms.toString());
//                        mParent.runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                mSpinnerAdapter.notifyDataSetChanged();
//                                mSpinner_AddGames.setSelection(mPlatformIds.indexOf("7"));
//                            }
//                        });
//                } catch (JSONException e) {
//                    Log.e("JSON exception", e.getMessage());
//                    e.printStackTrace();
//                }
//            }
//        });
//    }

//    public void setUpSpinner(){
//        mSpinnerAdapter = new ArrayAdapter<String>(mParent, android.R.layout.simple_spinner_item, mPlatforms);
//        mSpinner_AddGames.setAdapter(mSpinnerAdapter);
//        mSpinner_AddGames.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                String platformId = mPlatformIds.get(i);
//
//                mSearchView_AddGames.setQuery("", false);
//                mGames.clear();
//                mFilteredGames.clear();
//                mGameAdapter.notifyDataSetChanged();
//
//                if(mGameListStorage.get(platformId) == null){
//                    Log.d("Fetching", "From GamesDB");
//                    findGamesByPlatform(platformId);
//                }else{
//                    Log.d("Fetching", "From Local Storage, id " + platformId);
//                    for(Game game : mGameListStorage.get(platformId)){
//                        mGames.add(game);
//                        mFilteredGames.add(game);
//                    }
//
//                    mGameAdapter.notifyDataSetChanged();
//                }
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
//    }

//    public void findGamesByPlatform(final String platformId){
//        final GamesDbService apiService = new GamesDbService();
//
//        apiService.findGamesByPlatform(platformId, new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                mParent.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(mParent, "Unable to fetch games from GamesDB", Toast.LENGTH_SHORT).show();
//                    }
//                });
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//                JSONObject jsonObj = null;
//                try {
//                    jsonObj = XML.toJSONObject(response.body().string());
//                    Log.d("GamesByPlatformObject:", jsonObj.toString());
//                    JSONArray rawGames = jsonObj.getJSONObject("Data").getJSONArray("Game");
//                    for(int i = 0; i < rawGames.length(); i++){
//                        JSONObject data = rawGames.getJSONObject(i);
//                        String name = data.getString("GameTitle");
//                        String id = data.getString("id");
//                        if(name != null && id != null){
//                            Game game = new Game(name, id);
//                            mGames.add(game);
//                        }
//                    }
//                    mParent.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Collections.sort(mGames, new Comparator<Game>() {
//                                @Override
//                                public int compare(Game game, Game t1) {
//                                    return game.getName().compareTo(t1.getName());
//                                }
//                            });
//                            mGameListStorage.put(platformId, (ArrayList<Game>) mGames.clone());
//
//                            for(Game game : mGames){
//                                mFilteredGames.add(game);
//                            }
//                            mGameAdapter.notifyDataSetChanged();
//                        }
//                    });
//                } catch (JSONException e) {
//                    Log.e("JSON exception", e.getMessage());
//                    e.printStackTrace();
//                }
//            }
//        });
//    }
}
