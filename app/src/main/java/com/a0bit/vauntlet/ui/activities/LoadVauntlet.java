package com.a0bit.vauntlet.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.a0bit.vauntlet.Constants;
import com.a0bit.vauntlet.R;
import com.a0bit.vauntlet.adapters.GameListAdapter;
import com.a0bit.vauntlet.adapters.VauntletListAdapter;
import com.a0bit.vauntlet.models.Vauntlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoadVauntlet extends AppCompatActivity {
    @BindView(R.id.recyclerView_Vauntlets) RecyclerView mRecyclerView_Vauntlets;

    private VauntletListAdapter mVauntletListAdapter;
    private List<Vauntlet> mVauntlets = new ArrayList<Vauntlet>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_vauntlet);
        ButterKnife.bind(this);

        fetchVauntlets();
    }

    private void fetchVauntlets(){
                File file = getFileStreamPath(Constants.FILE_VAUNTLETS);
        FileInputStream fis = null;
        try {
            fis = openFileInput(Constants.FILE_VAUNTLETS);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            if(!file.createNewFile()){
                Log.d(">>", "File existed");
                if(file.length() != 0){
                    Log.d("Length of file:", file.length() + "");
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    mVauntlets = (ArrayList<Vauntlet>) ois.readObject();
                    ois.close();
                    Log.v("TAG", mVauntlets.size() + "");
                    setUpRecyclerView();
                }
            }else{
                Log.d(">>", "File did not exist");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        for(Vauntlet v : mVauntlets){
            Log.v("TAG", v.getName());
        }
    }

    public void setUpRecyclerView(){
        mVauntletListAdapter = new VauntletListAdapter(mVauntlets, this);
        mRecyclerView_Vauntlets.setHasFixedSize(true);
        mRecyclerView_Vauntlets.setAdapter(mVauntletListAdapter);
        mRecyclerView_Vauntlets.setLayoutManager(new LinearLayoutManager(this));

    }
}
