package com.a0bit.vauntlet.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.a0bit.vauntlet.Constants;
import com.a0bit.vauntlet.R;
import com.a0bit.vauntlet.models.Vauntlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    @BindView(R.id.button_NewVauntlet) Button mButton_NewVauntlet;
    @BindView(R.id.button_LoadVauntlet) Button mButton_LoadVauntlet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);



        File file = getFileStreamPath(Constants.FILE_VAUNTLETS);
        FileInputStream fis = null;
        try {
            fis = openFileInput(Constants.FILE_VAUNTLETS);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ArrayList<Vauntlet> activeVauntlets = new ArrayList<>();

        try {
            if(!file.createNewFile()){
                Log.d(">>", "File existed");
                if(file.length() != 0){
                    Log.d("Length of file:", file.length() + "");
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    activeVauntlets = (ArrayList<Vauntlet>) ois.readObject();
                    ois.close();
                }
            }else{
                Log.d(">>", "File did not exist");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        for(Vauntlet v : activeVauntlets){
            Log.v(">>", v.getName());
        }

        mButton_NewVauntlet.setOnClickListener(this);
        mButton_LoadVauntlet.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view == mButton_NewVauntlet){
            startActivity(new Intent(this, CreateVauntlet.class));
        }else if(view == mButton_LoadVauntlet){
            startActivity(new Intent(this, LoadVauntlet.class));
        }

    }
}
