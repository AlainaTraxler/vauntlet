package com.a0bit.vauntlet.ui.fragments;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.a0bit.vauntlet.R;
import com.a0bit.vauntlet.adapters.GameListAdapter;
import com.a0bit.vauntlet.models.Game;
import com.a0bit.vauntlet.ui.activities.CreateVauntlet;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditVauntletFragment extends Fragment implements View.OnClickListener{
    @BindView(R.id.recyclerView_EditVauntlet) RecyclerView mRecyclerView_EditVauntlet;
    @BindView(R.id.floatingActionButton_CreateVauntlet) FloatingActionButton mFloatingActionButton_CreateVauntlet;
    @BindView(R.id.textView_VauntletName) EditText mEditText_VauntletName;

    private ArrayList<Game> mGameList = new ArrayList<Game>();
    private GameListAdapter mGameAdapter;
    private ArrayList<Game> mFilteredGames = new ArrayList<>();

    private CreateVauntlet mParent;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_edit_vauntlet, container, false);
        ButterKnife.bind(this, view);
        mParent = (CreateVauntlet) getActivity();

        mFilteredGames = mGameList;
        setUpRecyclerView();

        mFloatingActionButton_CreateVauntlet.setOnClickListener(this);

        return view;
    }

    public void updateGameListAdapter(){
        mGameAdapter.notifyDataSetChanged();
        Toast.makeText(mParent,  mGameList.get(mGameList.size() - 1).getName() + " added", Toast.LENGTH_SHORT).show();
    }

    public void setUpRecyclerView(){
        mGameAdapter = new GameListAdapter(mFilteredGames, mParent, "EditVauntletFragment");
        mRecyclerView_EditVauntlet.setHasFixedSize(true);
        mRecyclerView_EditVauntlet.setAdapter(mGameAdapter);
        mRecyclerView_EditVauntlet.setLayoutManager(new LinearLayoutManager(this.getContext()));
    }

    public void pairGameList(ArrayList<Game> _gameList){
        mGameList = _gameList;
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(mParent, "Clicked", Toast.LENGTH_SHORT).show();
        try {
            if(verifyField()){
                mParent.createVauntlet(mEditText_VauntletName.getText().toString().trim());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public boolean verifyField(){
        String name = mEditText_VauntletName.getText().toString();
        if(name == null || name.replace(" ", "").equals("")){
            Toast.makeText(mParent, "Please enter a name for your Vauntlet", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }
    }
}
