package com.a0bit.vauntlet.services;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.a0bit.vauntlet.Constants;
import com.a0bit.vauntlet.models.Game;
import com.a0bit.vauntlet.ui.fragments.AddGamesFragment;
import com.android.volley.VolleyError;
import com.igdb.api_android_java.callback.onSuccessCallback;
import com.igdb.api_android_java.model.APIWrapper;
import com.igdb.api_android_java.model.Parameters;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Alaina Traxler on 3/8/2018.
 */

public class IgdbService {
    private Context mParent;
    private Dialog mDialog;

    public ArrayList<Game> searchForGame(String _searchString, Context _context, final AddGamesFragment _fragment){
        final ArrayList<Game> foundGames = new ArrayList<>();
        mParent = _context;

        Toast.makeText(mParent, "Searching for " + _searchString, Toast.LENGTH_SHORT).show();

        APIWrapper wrapper = new APIWrapper(mParent, Constants.KEY_IGDB);
        Parameters params = new Parameters()
                .addSearch(_searchString)
                .addFields("name, platforms");

        wrapper.search(APIWrapper.Endpoint.GAMES, params, new onSuccessCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                _fragment.mGames.clear();
                for(int i = 0; i<result.length(); i++){
                    try{
                        JSONObject obj = result.getJSONObject(i);
                        _fragment.mGames.add(new Game(obj.getString("name"),obj.getString("id")));
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }

                _fragment.mGameAdapter.notifyDataSetChanged();

                Log.v("TAG", result.toString());
            }

            @Override
            public void onError(VolleyError error) {
                // Do something on error
                Toast.makeText(mParent, "Search failed", Toast.LENGTH_SHORT).show();
                error.printStackTrace();
            }
        });

        return foundGames;
    }
}
