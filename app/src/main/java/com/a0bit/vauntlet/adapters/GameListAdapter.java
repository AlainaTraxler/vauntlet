package com.a0bit.vauntlet.adapters;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.a0bit.vauntlet.R;
import com.a0bit.vauntlet.models.Game;
import com.a0bit.vauntlet.ui.activities.CreateVauntlet;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaina Traxler on 2/9/2018.
 */

public class GameListAdapter extends RecyclerView.Adapter<GameListAdapter.GameViewHolder> {
    List<Game> mGames = new ArrayList<>();
    Activity mParent;
    Typeface PressStart;
    private String mLocation;

    public GameListAdapter(List<Game> gamelets, Activity parent, String _location){
        mGames = gamelets;
        mParent = parent;
        this.mLocation = _location;
    }

    @Override
    public GameListAdapter.GameViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_gamelet, parent, false);
        GameViewHolder viewHolder = new GameViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(GameListAdapter.GameViewHolder holder, int position) {
        holder.bindGame(mGames.get(position));

    }

    @Override
    public int getItemCount() {
        return mGames.size();
    }

    public class GameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.textView_Name) TextView mTextView_Name;
        @BindView(R.id.imageView_AddGame) ImageView mImageView_AddGame;

        private View mItemView;

        public GameViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mItemView = itemView;
//            PressStart = Typeface.createFromAsset(mParent.getAssets(), "fonts/PressStart2P-Regular.ttf");

            itemView.setOnClickListener(this);
            mImageView_AddGame.setOnClickListener(this);

        }

        public void bindGame(Game game){
            mTextView_Name.setTypeface(PressStart);
            mTextView_Name.setText(game.getName());

            switch (mLocation){
                case "AddGamesFragment":
                    mImageView_AddGame.setVisibility(View.VISIBLE);
                    break;
                case "EditVauntletFragment":
                    break;
                default: break;
            }
        }

        @Override
        public void onClick(View view) {
            Game game = mGames.get(getAdapterPosition());
            if(view == mImageView_AddGame){
                ((CreateVauntlet) mParent).addGame(game);
                //fetchGame(game.getGameId(), "add");
            }else if(view == mItemView){
                //fetchGame(game.getGameId(), "display");
            }
        }

        public void fetchGame(final String query, final String _behavior){
//            final GamesDbService apiService = new GamesDbService();
//            apiService.findGameById(query, new Callback() {
//                @Override
//                public void onFailure(Call call, IOException e) {
//                    mParent.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(mParent, "Unable to fetch game from GamesDB", Toast.LENGTH_SHORT).show();
//                        }
//                    });
//                }
//
//                @Override
//                public void onResponse(Call call, Response response) throws IOException {
////                    JSONObject jsonObj = null;
//                    final Game game = apiService.processResultById(response, query, mParent);
//                    if(_behavior.equals("add")){
//                        mParent.runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                mParent.addGame(game);
//                            }
//                        });
//                    }
//                }
//            });
        }
    }
}
