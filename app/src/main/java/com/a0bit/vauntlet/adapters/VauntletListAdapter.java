package com.a0bit.vauntlet.adapters;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.a0bit.vauntlet.R;
import com.a0bit.vauntlet.models.Vauntlet;
import com.a0bit.vauntlet.models.Vauntlet;
import com.a0bit.vauntlet.ui.activities.CreateVauntlet;
import com.a0bit.vauntlet.ui.activities.LoadVauntlet;
import com.a0bit.vauntlet.ui.activities.ShowVauntlet;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Alaina Traxler on 3/12/2018.
 */

public class VauntletListAdapter extends RecyclerView.Adapter<VauntletListAdapter.VauntletViewHolder> {
    List<Vauntlet> mVauntlets = new ArrayList<>();
    //CreateVauntlet mParent;
    Typeface PressStart;
    LoadVauntlet mParent;

    public VauntletListAdapter(List<Vauntlet> vauntlets, LoadVauntlet parent){
        mVauntlets = vauntlets;
        mParent = parent;
    }

    @Override
    public VauntletListAdapter.VauntletViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_vauntlet, parent, false);
        VauntletListAdapter.VauntletViewHolder viewHolder = new VauntletListAdapter.VauntletViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(VauntletListAdapter.VauntletViewHolder holder, int position) {
        holder.bindVauntlet(mVauntlets.get(position));
    }

    @Override
    public int getItemCount() {
        return mVauntlets.size();
    }

    public class VauntletViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        @BindView(R.id.textView_Name)
        TextView mTextView_Name;

        private View mItemView;

        public VauntletViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            mItemView = itemView;
//            PressStart = Typeface.createFromAsset(mParent.getAssets(), "fonts/PressStart2P-Regular.ttf");

            itemView.setOnClickListener(this);

        }

        public void bindVauntlet(Vauntlet vauntlet){
            mTextView_Name.setTypeface(PressStart);
            mTextView_Name.setText(vauntlet.getName());
        }

        @Override
        public void onClick(View view) {
            Vauntlet vauntlet = mVauntlets.get(getAdapterPosition());
            mParent.startActivity(new Intent(mParent, ShowVauntlet.class).putExtra("VAUNTLET", vauntlet));
        }
    }
}
