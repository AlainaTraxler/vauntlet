package com.a0bit.vauntlet.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Alaina Traxler on 2/11/2018.
 */

public class Vauntlet implements Serializable{
    public String name;
    public Calendar createdOn;
    public Calendar endsOn;
    public ArrayList<Game> gameList;
    public boolean complete;

    public Vauntlet(String name, ArrayList<Game> gameList) {
        this.name = name;
//        this.endsOn = endsOn;
        this.gameList = gameList;
        this.complete = false;
        //TODO fetch createdOn
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Calendar createdOn) {
        this.createdOn = createdOn;
    }

    public Calendar getEndsOn() {
        return endsOn;
    }

    public void setEndsOn(Calendar endsOn) {
        this.endsOn = endsOn;
    }

    public ArrayList<Game> getGameList() {
        return gameList;
    }

    public void setGameList(ArrayList<Game> gameList) {
        this.gameList = gameList;
    }

    public boolean isComplete() {
        return complete;
    }

    public void setComplete(boolean complete) {
        this.complete = complete;
    }
}
