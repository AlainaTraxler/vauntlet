# README #

### What is this repository for? ###

* A quick, knock-the-cobwebs-out project that ran into API limitations. For the curious, GamesDB, while free, ran into some serious slowdown issues, and IGDB, while fast and very complete, did not have the ability to pull full lists of games or produce a random one. So, the whole thing became a chance to experiment with the IGDB API, methods of moving data into and out of fragments and adapters, and writing files to Android.

* Functionally, this will let you search the IGDB APU for a game, add it to a Vauntlet (video game + gauntlet) and then save and display said Vauntlet. Not very exciting, but a decent leg stretch for the follow-up project: ScoreBash.

* Version: Alpha